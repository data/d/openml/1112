# OpenML dataset: KDDCup09_churn

https://www.openml.org/d/1112

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Orange Telecom  
**Source**: [ACM KDD Cup](http://www.sigkdd.org/kddcup/index.php) - 2009  
**Please cite**: 

The KDD Cup 2009 offers the opportunity to work on large marketing databases from the French Telecom company Orange to predict the propensity of customers to switch provider (churn). 

Churn (wikipedia definition): Churn rate is also sometimes called attrition rate. It is one of two primary factors that determine
the steady-state level of customers a business will support. In its broadest sense, churn rate is a measure of the number
of individuals or items moving into or out of a collection over a specific period of time.

The term is used in many contexts, but is most widely applied in business with respect to a contractual customer base. For instance, it is an important factor for any business with a subscriber-based service model, including mobile telephone networks and pay TV operators. The term is also used to refer to participant turnover in peer-to-peer networks.

The training set contains 50,000 examples.
The first predictive 190 variables are numerical and the last 40 predictive variables are categorical.
The last target variable is binary {-1,1}.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1112) of an [OpenML dataset](https://www.openml.org/d/1112). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1112/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1112/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1112/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

